package midtermV1;

public class Task2 {
    public static void main(String[] args) {
        LinkedList Linkedlist = new LinkedList();

        Linkedlist.insertAtFirst(8);
        Linkedlist.insertAtFirst(7);
        Linkedlist.insertAtFirst(6);
        Linkedlist.insertAtFirst(5);
        Linkedlist.insertAtFirst(4);
        Linkedlist.insertAtFirst(3);
        Linkedlist.insertAtFirst(2);
        Linkedlist.insertAtFirst(1);

        Linkedlist.removeAtPosition(2);

        Linkedlist.showList();
    }

}
