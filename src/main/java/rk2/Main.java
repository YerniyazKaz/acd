package rk2;

public class Main {
    public static void main(String[] args)
    {

        Node.Task1 tree = new Node.Task1();
        tree.root = new Node(2);
        tree.root.left = new Node(1);
        tree.root.right = new Node(3);
        tree.root.right.left = new Node(4);
        tree.root.right.right = new Node(5);
        tree.root.right.right.right = new Node(6);


        System.out.println("(4, 5)   ----> "+ tree.findPath(4,5));
        System.out.println("(4, 6)   ---->" + tree.findPath(4,6));
        System.out.println("(3, 4)   ---->" + tree.findPath(3,4));
        System.out.println("(2, 4)   ----> " + tree.findPath(2,4));


    }
}

