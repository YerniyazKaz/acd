package rk2;

public class Task3 {
    class Node {
        int data;
        Node left, right;

        Node(int d) {
            data = d;
            left = right = null;
        }
    }

     class BinaryTree {

       public Node head;


        Node insert(Node node, int data) {


            if (node == null) {
                return (new Node(data));
            } else {


                if (data <= node.data) {
                    node.left = insert(node.left, data);
                } else {
                    node.right = insert(node.right, data);
                }


                return node;
            }
        }


        int minvalue(Node node) {
            Node current = node;


            while (current.left != null) {
                current = current.left;
            }
            return (current.data);
        }
    }
}
