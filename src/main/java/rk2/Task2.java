package rk2;

public class Task2 {

    static class Node {
        int key;
        Node left, right;
    }


    static Node newNode(int item)
    {
        Node temp = new Node();
        temp.key = item;
        temp.left = null;
        temp.right = null;
        return temp;
    }


    static void inorder(Node root)
    {
        if (root != null) {
            inorder(root.left);
            System.out.print(root.key + " ");
            inorder(root.right);
        }
    }


    static Node insert(Node node, int key)
    {

        if (node == null)
            return newNode(key);


        if (key < node.key)
            node.left = insert(node.left, key);
        else
            node.right = insert(node.right, key);

        return node;
    }


    static void countOdd(Node root)
    {
        if (root != null) {
            countOdd(root.left);

            // if node is odd then print it
            if (root.key % 2 != 0)
                System.out.print(root.key + " ");

            countOdd(root.right);
        }
    }
}
