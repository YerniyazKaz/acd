package rk2;

import java.util.ArrayList;
import java.util.List;

    public  class Node {
        int data;
        Node left, right;

        Node(int value) {
            data = value;
            left = right = null;
        }

        public static class Task1 {

            Node root;
            private List<Integer> way1 = new ArrayList<>();
            private List<Integer> way2 = new ArrayList<>();


            int findPath(int n1, int n2) {
                way1.clear();
                way2.clear();
                return nodeica(root, n1, n2);
            }

            private int nodeica(Node root, int value1, int value2) {

                if (!findWay(root, value1, way1) || !findWay(root, value2, way2)) {
                    System.out.println((way1.size() > 0) ? "n1 is present" : "n1 is missing");
                    System.out.println((way2.size() > 0) ? "n2 is present" : "n2 is missing");
                    return -1;
                }

                int i;
                for (i = 0; i < way1.size() && i < way2.size(); i++) {


                    if (!way1.get(i).equals(way2.get(i)))
                        break;
                }

                return way1.get(i - 1);
            }

            private boolean findWay(Node root, int n, List<Integer> path) {

                if (root == null) {
                    return false;
                }


                path.add(root.data);

                if (root.data == n) {
                    return true;
                }

                if (root.left != null && findWay(root.left, n, path)) {
                    return true;
                }

                if (root.right != null && findWay(root.right, n, path)) {
                    return true;
                }

                path.remove(path.size() - 1);

                return false;
            }

        }
    }




