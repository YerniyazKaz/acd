package kz.aitu.week3.quiz;

public class LinkedListTest {


        public static void main(String args[]) {
            //creating LinkedList with 5 elements including head
            LinkedList linkedList = new LinkedList();
            LinkedList.Node head = linkedList.head();
            linkedList.add( new Node("1"));
            linkedList.add( new Node("2"));
            linkedList.add( new Node("3"));
            linkedList.add( new Node("4"));


            Node middle=null;
            LinkedList.Node current=head;
            int length = 30;
            while (current.next() !=null){
                length++;
                if (length%2 ==0){
                    middle=middle.next();
                    current= current.next();
                }
            }
            if(length%2 == 1){
                middle = middle.next();
            }
            //finish here

            System.out.println("length of LinkedList: " + length);
            System.out.println("middle element of LinkedList : "                                  + middle);

        }

    }

