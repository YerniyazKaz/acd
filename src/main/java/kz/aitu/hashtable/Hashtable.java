package kz.aitu.hashtable;

public class Hashtable {

    public Node table[];
    public int size;

    public Hashtable(int size) {
        table = new Node[size];
        this.size = size;
    }

    public void set(String key, String value) {
        Node node = new Node(key, value);
        int index = key.hashCode() % size;

        if(table[index] == null){
            table[index] = node;
        }
        else{
            Node current = table[index];
            while (current != null) {

                if (current.getKey() == key) {
                    current.setValue(value);
                    return;
                }
                if (current.getNext() == null) {
                    current.setNext(node);
                    return;
                }
                current = current.getNext();
            }
        }

    }


    public String get(String key) {
        int index = key.hashCode() % size;
        Node current = table[index];
        while(current != null){
            if (current.getKey() == key) {
                return table[index].getValue();
            }
            current = current.getNext();
        }
        return null ;

    }


    public void remove(String key) {
        table[key.hashCode() % size] = null;





    }

    public int getSize() {
        return size;
    }

    public void printAll() {
        for (int i = 0; i < table.length; i++) {
            Node j = table[i];

            System.out.print(i + ": ");
            while(j != null) {
                System.out.print(j.getKey() + "-" + j.getValue() + "\t\t>");
                j = j.getNext();
            }
            System.out.println();
        }
    }
    }

