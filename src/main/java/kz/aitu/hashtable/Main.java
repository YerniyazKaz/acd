package kz.aitu.hashtable;

public class Main {

    public static void main(String[] args) {
        Hashtable hashTable = new Hashtable(10);

        hashTable.set("0", "IPHONE");
        hashTable.set("0", "ADNROID");
        hashTable.set("2", "SUMSUNG");
        hashTable.set("3", "ILON MUSK");
        hashTable.set("4", "TESLA");
        hashTable.set("5", "XPAY");
        hashTable.set("6", "TRACK");
        hashTable.set("7", "MAC");
        hashTable.set("8", "JAVA");
        hashTable.set("LOL", "LOL");
        hashTable.set("game", "D2");
        hashTable.set("ky", "ky");
        hashTable.set("lol", "cheburek");

        System.out.println("===PRINTED JAVA (value of '8')");
        System.out.println(hashTable.get("0" +
                ""));
        System.out.println("===PRINTED JAVA");

        System.out.println("===PRINTING ALL ELEMENTS: ");
        hashTable.printAll();
        System.out.println("===ALL ELEMENTS PRINTED!");



        System.out.println("===REMOVING key game: ");
        hashTable.remove("game");
        hashTable.printAll();
        System.out.println("===ALL ELEMENTS PRINTED! without game");


        System.out.println("===CHANGE value of 'LOL' ELEMENT: ");
        hashTable.set("lol", "CHANGED");
        hashTable.printAll();
        System.out.println("===ALL ELEMENTS PRINTED! must be printed CHANGED");
    }
}
